/** @jsx React.DOM */

var FormInfosView = React.createClass({

	getInitialState: function() {
		return {
			userInfos: this.props.userInfos
		};
	},

	onChange: function() {
		var userInfos = {
			firstName: this.refs.firstName.getDOMNode().value,
			lastName: this.refs.lastName.getDOMNode().value,
			tel: this.refs.tel.getDOMNode().value
		};

		this.setState({
			userInfos: userInfos
		});
	},


	onClick: function() {
		this.props.onValidate(this.state.userInfos);
		return false;
	},

	render: function() {
		return (
			<div>
				<label className="label" for="firstName">Nom</label>
				<input id="firstName" type="text" name="firstName" placeholder="Entrez le nom" className="input col-md-12" ref="firstName" onChange={this.onChange} value={this.state.userInfos.firstName} />

				<label className="label" for="lasttName">Prénom</label>
				<input id="lastName" type="text" name="lastName" placeholder="Entrez le prénom" className="input col-md-12" ref="lastName" onChange={this.onChange} value={this.state.userInfos.lastName} />

				<label className="label" for="tel">Téléphone</label>
				<input id="tel" type="tel" name="tel" placeholder="Entrez le numéro" className="input col-md-12" ref="tel" value={this.props.userInfos.tel} />

				<button className="button button-success" onClick={this.onClick}>Valider</button>
			</div>
		);
	}

});
