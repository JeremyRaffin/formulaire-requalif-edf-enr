/** @jsx React.DOM */

var Questionnaire = React.createClass({

	getInitialState: function() {
		return {
			step: 'q1',
			userInfos: userInfos
		};
	},


	onValidateQuestion1: function(index) {
		var nextStep = 'q2';
		if(index === 3) {
			nextStep = 'refuse';
		} else if (index === 1) {
			nextStep = 'rappel';
		} else if (index === 2) {
			nextStep = 'identiteVerif';
		}

		this.setState({
			step: nextStep
		});
	},

	onValidateQuestionIdentite: function(index) {
		if(index === 0) {
			nextStep = 'q2';
		} else if(index === 1) {
			nextStep = 'identiteDispo';
		}

		this.setState({
			step: nextStep
		});
	},

	onValidateQuestionDispo: function(index) {
		if(index === 0) {
			nextStep = 'q2';
		} else if(index === 1) {
			nextStep = 'rappel';
		}

		this.setState({
			step: nextStep
		});
	},
	
	onValidateQuestion2: function(index) {
		var nextStep = 'q3';
		if(index === 1) {
			nextStep = 'refuse';
		}

		this.setState({
			step: nextStep
		});
	},
	
	onValidateQuestion3: function(index) {
		var nextStep = 'formulaire';
		if(index === 1) {
			nextStep = 'refuse';
		}

		this.setState({
			step: nextStep
		});
	},
	
	onValidateForm: function(userInfos) {
		this.setState({
			userInfos: userInfos,
			step: 'valide'
		});
	},


	renderStep: function() {

		switch(this.state.step) {
			case 'q1':
				return <Question1View userInfos={this.state.userInfos} onValidateQuestion={this.onValidateQuestion1} />
				break;
			case 'identiteVerif':
				return <QuestionIdentiteView userInfos={this.state.userInfos} onValidateQuestion={this.onValidateQuestionIdentite} />
				break;
			case 'identiteDispo':
				return <QuestionDispoView userInfos={this.state.userInfos} onValidateQuestion={this.onValidateQuestionDispo} />
				break;
			case 'q2':
				return <Question2View onValidateQuestion={this.onValidateQuestion2} />
				break;
			case 'q3':
				return <Question3View onValidateQuestion={this.onValidateQuestion3} />
				break;
			case 'formulaire':
				return 	<div className="col-md-12">
							<p><strong>Parfait, il me reste simplement à confirmer vos coordonnées.</strong></p>
							<FormInfosView onValidate={this.onValidateForm} userInfos={this.state.userInfos} />
						</div>
				break;
			case 'rappel':
				return <div className="col-md-12">
							<button className="button button-primary">Planifier un rappel</button>
						</div>;
				break;
			case 'valide':
				return <div className="col-md-12"><p>Je vous confirme donc qu’un conseiller EDF ENR, vous contactera dans les 48h. Je vous remercie et vous souhaite une excellente fin de journée.</p></div>;
				break;
			case 'refuse':
				return <div className="col-md-12">
							<p>Dans ce cas je prends note et j’annule la demande d’information je vous souhaite une excellente fin de journée</p>
							<button className="button button-danger">Disqualifier</button>
						</div>;
				break;
			default:
				return;
				break;

		}

		return;
	},

	render: function() {
		return (
			<form role="form" className="form m-t-m m-b-m row" method="post" action="">
				{this.renderStep()}
			</form>
		);
	}
});