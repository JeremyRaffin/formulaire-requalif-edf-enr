/** @jsx React.DOM */

var Question3View = React.createClass({
  render: function() {

    var question = 'Vous êtes bien propriétaire d\'une maison individuelle ?';
    var answers = [
      'Oui',
      'Non',
    ];

    return (
      <QuestionView question={question} answers={answers} onValidateQuestion={this.props.onValidateQuestion} />
    );
  }
});   