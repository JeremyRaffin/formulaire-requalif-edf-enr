/** @jsx React.DOM */

var QuestionIdentiteView = React.createClass({
	render : function () {

		var question = 'Vous êtes bien ' + this.props.userInfos.firstName + ' ' + this.props.userInfos.lastName + '?';
		var answers = [
			'Oui',
			'Non'
		];

		return (
			<QuestionView question={question} answers={answers} onValidateQuestion={this.props.onValidateQuestion} />
		);
	}

});