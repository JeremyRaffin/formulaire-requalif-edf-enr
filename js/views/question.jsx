/** @jsx React.DOM */

var QuestionView = React.createClass({

  getInitialState: function() {
    return {
      error: null
    };
  },

  onClick: function() {
    var keys = Object.keys(this.refs);
    var self = this;

    var result = keys.map(function(key) {
      return self.refs[key].getDOMNode().checked;
    });

    var index = result.indexOf(true);

    if(index === -1) {
      this.setState({
        error: 'Vous devez sélectionner une réponse avant de valider'
      });
      return false;
    }

    this.props.onValidateQuestion(index);

    return false;
  },

  renderAnswer: function(answer, index) {
    return (
      <div className="row">
        <div className="radio col-md-12">
          <label className="label label-radio" for={'answer-' + index}>
            <input id={'answer-' + index} name={'answer'} ref={'answer-' + index} type="radio" />
            &nbsp;
            {answer}
          </label>
        </div>
      </div>
    );
  },

  renderError: function() {
  return <p className="p text-danger"><strong>{this.state.error}</strong></p>;
  },

  render: function() {
    return (
      <div className="col-md-12">
        <p className="p">
          <strong>{this.props.question}</strong>
        </p>
        {this.state.error !== null ? this.renderError() : ''}
        {this.props.answers.map(this.renderAnswer)}
        <button className="button button-primary" onClick={this.onClick}>Valider</button>
      </div>
    );          

  }
});