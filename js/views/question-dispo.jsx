/** @jsx React.DOM */

var QuestionDispoView = React.createClass({
	render : function () {

		var question = 'M. ou Mme ' + this.props.userInfos.firstName + ' ' + this.props.userInfos.lastName + ' est-il disponible ?';
		var answers = [
			'Oui',
			'Non'
		];

		return (
			<QuestionView question={question} answers={answers} onValidateQuestion={this.props.onValidateQuestion} />
		);
	}


});