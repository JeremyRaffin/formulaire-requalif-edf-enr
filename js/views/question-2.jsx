/** @jsx React.DOM */

var Question2View = React.createClass({
	render: function() {

		var question = 'Je suis [prénom], et je suis chargé(e) de vérifier votre souhait d’être rappelé par un conseillé EDF ENR, expert sur le marché des photovoltaïques. Me confirmez-vous votre intérêt ? :';
		var answers = [
			'Oui',
			'Non',
		];

		return (
			<QuestionView question={question} answers={answers} onValidateQuestion={this.props.onValidateQuestion} />
		);
	}
});