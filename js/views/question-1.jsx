/** @jsx React.DOM */

var Question1View = React.createClass({
	render: function() {

		var question = 'Bonjour, je souhaiterais parler à M. ou Mme ' + this.props.userInfos.firstName + ' ' + this.props.userInfos.lastName + ', vous avez déposé sur Internet une demande pour être rappelé par un conseiller EDF ENR (pour vous aider à maîtriser vos dépenses énergétiques), c’est bien ça ?';
		var answers = [
			'Oui c’est bien moi',
			'Oui c’est bien moi mais pouvez-vous me rappeler à un autre moment',
			'Non, ça me dit rien',
			'Oui c’est bien moi mais je ne suis plus intéressé'
		];

		return (
			<QuestionView question={question} answers={answers} onValidateQuestion={this.props.onValidateQuestion} />
		);
	}
});