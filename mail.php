<?php
if(isset($_POST['submit'])) 
{

$message=
'Nom: '.$_POST['nom'].'<br />
Sujet:  '.$_POST['subject'].'<br />
Téléphone:  '.$_POST['tel'].'<br />
Email:  '.$_POST['email'].'<br />
Message: '.$_POST['message'].'
';
    include "phpmailer/class.phpmailer.php"; //include phpmailer class
    include "phpmailer/class.smtp.php";
      
    // Instantiate Class  
    $mail = new PHPMailer();  
      
    // Set up SMTP  

    $mail->IsSMTP();                // Sets up a SMTP connection  
    $mail->SMTPAuth = true;         // Connection with the SMTP does require authorization    
    $mail->SMTPSecure = "ssl";      // Connect using a TLS connection  
    $mail->Host = "smtp.gmail.com";  //Gmail SMTP server address
    $mail->Port = 465;  //Gmail SMTP port
    $mail->Encoding = '7bit';
    
    // Authentication  
    $mail->Username   = "john.doe@gmail.com"; // Your full Gmail address
    $mail->Password   = "password"; // Your Gmail password
      
    // Compose
    $mail->SetFrom($_POST['email'], $_POST['nom']);
    $mail->AddReplyTo($_POST['email'], $_POST['nom']);
    $mail->Subject = "Start project - Nouveau contact";      // Subject (which isn't required)  
    $mail->MsgHTML($message);
 
    // Send To  
    $mail->AddAddress("john.doe@gmail.com", "John Doe"); // Where to send it - Recipient
    $result = $mail->Send();    // Send!  
  $message = $result ? 'Votre message a été envoyé, nous vous contacterons prochainement, merci.' : 'Votre message n\'a pas pu être correctement envoyé, veuillez réessayer ou contactez-nous via les coordonnées ci-dessus, merci.';      
  unset($mail);

}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Contact - Company</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/main.css"><!--[if lt IE 9]>
    <script src="js/ltie9/min/ltie9.min.js"></script><![endif]-->
  </head>
  <body>
    <header role="banner" class="header fixed p-t-m p-b-m">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <h1 class="header-logo-h1"> <a href="index.html" title="Haratryn"> <img alt="Logo" src="img/haratryn.png" class="header-logo-img img"></a></h1>
          </div>
          <div class="col-md-8">
            <nav role="navigation" class="primary-nav right m-t-xs">
              <ul class="list-inline primary-nav-ul">
                <li class="primary-nav-ul-item"><a href="ingenieur.html" title="Ingénieur info partagé" class="primary-nav-ul-item-a">Ingénieur info partagé</a></li>
                <li class="primary-nav-ul-item"> <a href="infogerance.html" title="Infogérance" class="primary-nav-ul-item-a">Infogérance</a></li>
                <li class="primary-nav-ul-item"> <a href="savoir-faire.html" title="Savoir faire" class="primary-nav-ul-item-a">Savoir faire</a></li>
                <li class="primary-nav-ul-item"> <a href="contact.html" title="Contact" class="primary-nav-ul-item-a button button-light button-s button-nav">Contact</a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
    <section class="section section-bg-faire-bon-choix">
      <div class="container">
        <div class="row">
          <h2 class="h2 text-center m-b-s">Contactez-nous</h2>
          <div class="col-md-8 col-md-offset-2 text-center">
            <p class="lead m-t-s m-b-m">Contactez-nous via les coordonnées ou le formulaire ci-dessous pour commencer votre projet maintenant. </p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 col-md-offset-3 text-center">
            <h3 class="h3 m-t-m">Téléphone</h3>
            <p>+ 33 01 02 03 04 06</p>
          </div>
          <div class="col-md-3 text-center">
            <h3 class="h3 m-t-m">E-mail</h3>
            <p>haratryn@gmail.com</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <form role="form" id="contact" method="post" action="mail.php" novalidate class="form m-t-m">
            <p class="lead text-success"><?php if(!empty($message)) echo $message; ?></p>
              <div class="row">
                <div class="col-md-12 control-group">
                  <label for="name" class="label">Nom <small>(requis)</small></label>
                  <input id="name" type="text" name="nom" placeholder="Votre nom" data-validation-required-message="Veuillez entrer votre nom" required class="input block-100">
                  <p class="help-block"></p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 control-group">
                  <label for="email" class="label">Email <small>(requis)</small></label>
                  <input id="email" type="email" name="email" placeholder="Votre adresse email" data-validation-required-message="Veuillez entrer votre adresse email" required class="input block-100">
                  <p class="help-block"></p>
                </div>
                <div class="col-md-6 control-group">
                  <label for="tel" class="label">Téléphone</label>
                  <input id="tel" type="text" name="tel" placeholder="Votre numéro de téléphone" class="input block-100">
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 control-group">
                  <label for="message" class="label">Message <small>(requis)</small></label>
                  <textarea id="message" name="message" type="text" placeholder="Votre message" data-validation-required-message="Veuillez entrer votre message" required class="textarea block-100"></textarea>
                  <p class="help-block"></p>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 text-center m-t-s">
                  <button name="submit" type="submit" class="button button-light button-l button-secondary">Envoyer</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <footer role="contentinfo" class="footer p-t-m p-b-m">
      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <p class="footer-copyright-p">© 2014 Haratryn</p>
          </div>
          <div class="col-md-8">
            <nav class="footer-nav text-center">
              <ul class="footer-nav-ul list-inline">
                <li> <a href="index.html" title="Accueil">Accueil</a></li>
                <li> <a href="ingenieur.html" title="Ingénieur information partagé">Ingénieur information partagé</a></li>
                <li> <a href="infogerance.html" title="Infogérance">Infogérance</a></li>
                <li> <a href="savoir-faire.html" title="Savoir faire">Savoir faire</a></li>
                <li> <a href="contact.html" title="Contact">Contact</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-md-2">
            <nav class="footer-social-nav right">
              <ul class="footer-social-nav-ul list-inline">
                <li> <a href="#" title="Facebook"> <span class="icon-facebook"></span></a></li>
                <li> <a href="#" title="Twitter"> <span class="icon-twitter"></span></a></li>
                <li> <a href="#" title="Google Plus"> <span class="icon-googleplus"></span></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </footer>
    <script type="text/javascript" src="js/min/main.min.js"></script>
  </body>
</html>
